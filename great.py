#!/usr/bin/python
# python2 wannabe scivis or so
# usage: run -i great.py

from re import sub, search
from os.path import join
from pylab import *
from matplotlib.gridspec import GridSpec
ion();

loewe="/run/user/1000/gvfs/sftp:host=loewe"

stars = {
	"THC": loewe+"/home/astro/koeppel/simulations/ng-codecomp/lorene-thc/data_asc_1D/",
	"MHD": loewe+"/home/astro/koeppel/simulations/ng-codecomp/lorene-mhd-newer/data_asc_1D/"
}

# downloaded files
stars = {
#	"THC": "./loewe-lorenethc-data_asc_1D/",
	"TCH": "loewe-thc/lthc15-01-11-3-data_asc_1D/",
	"MHD": "loewe-thc/loewe-mhd-newer-data_asc_1D/",
}

# another comparison input
THCMHD = {
#	"THC1": "./loewe-lorenethc-data_asc_1D/",
#	"THC1": "./loewe-lorenethc16-1-9-data_asc_1D/",
	"2.THC": "./lthc15-01-11-3-data_asc_1D/",
#	"1.MHD": "./loewe-mhd-newer-data_asc_1D/",
	"1.MHD": "./loewe-mhd16-1-11-data_sc_1D/",
}

THCrotating = {
# actually I do not even want to compare something. Just
# want to display the profiles of a rotating initial data read in.
#	"RTHC":  "./loewe-ngr-16-1-15-data/",
	"RTHC": "loewe-thc/loewe-16-01-20-fgt0-data/",
	"NRTHC": "loewe-thc/lthc15-01-11-3-data_asc_1D/",
}

# rho press dens eps alp vel[0] tau

fname="rho.x.asc"
xaxis="x"

# 1D ASCII Cactus output format:
# time leve, refinement leve, multigrid level
names="1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data"
names = sub(r"[^a-z\s]", "", names).split()

show_timeslider = False

def dxy(d, xaxis="x", it=0):
	mask = logical_and(d["it"]==it, d["rl"]==0, d["ml"]==0)
	x,y = d[mask][xaxis], d[mask]["data"]
	return (x,y)

def compare(fname, stars=stars, xaxis=None):
	clf()
	extractAxis =  search(r"(x|y|z).asc", fname)
	if not xaxis and extractAxis:
		xaxis = extractAxis.group(1)
	else:
		print "Failed to extract axis from %s, please provide as arg." % fname
		return -1

	data = {}
	for star, data_dir in stars.iteritems():
		f = join(data_dir, fname)
		d = genfromtxt(f, names=names)
		data[star] = d

	# get common times
	mintime = max([min(d["it"]) for d in data.values()])
	maxtime = min([max(d["it"]) for d in data.values()])
	times = unique(flatten([d["it"] for d in data.values()]))
	times = times[logical_and(times >= mintime, times <= maxtime)]

	print "For %s in %s-axis from times %f to %f:" % (fname, xaxis, mintime, maxtime)
	print times

	def find_nearest(a, a0):
		return a.flat[abs(a-a0).argmin()]

	# plot the first time and store line references
	gs = GridSpec(2,1, height_ratios=[3,1])
	axMain = subplot(gs[0])
	axRatio = subplot(gs[1])
	if show_timeslider:
		subplots_adjust(left=0.25, top=0.85)
	else:
		subplots_adjust(left=0.05, top=0.95, right=0.95, bottom=0.05)
	title(fname)
	lines = {}
	initial_it=0
	for star, d in data.iteritems():
		x,y = dxy(d, xaxis, initial_it)
		l, = axMain.plot(x,y, "o-", label=star)
		lines[star] = l


	axMain.legend()
	axRatio.set_xlabel(xaxis)


	def draw(it_ideal=0):
		it = find_nearest(times, it_ideal)
		#print "Drawing for time it=%f => it=%f" % (it_ideal, it)
		for star, d in data.iteritems():
			x,y = dxy(d, xaxis, it)
			if(len(y)):
				lines[star].set_ydata(y) #plot(x,y, "o-", label=star)
		axMain.set_title("%s at t=%f" % (fname, it))

		# draw the ratio between both
		first = data.keys()[0]
		second = data.keys()[1]
		#print "Ratio: Showing %s/%s" % (first,second)

		x,y1 = dxy(data[first], xaxis, it)
		x,y2 = dxy(data[second], xaxis, it)
		if len(y1) and len(y2):
			axRatio.clear()
			eps = 1e-10
			ratio = y1/y2
			#print "Ratio at it=%f: %s" % (it, str(ratio))
			#x, ratio = x[y1>eps], ratio[y1>eps]
			axRatio.plot(x,ratio, "o-", label="%s/%s"%(first,second))

			# senseful or not: upper limit
			ylim = max(ratio)
			ylim = 5

			axRatio.set_ylim([0,ylim])
			axRatio.legend()

			# print some values
			ratio_values = unique(ratio)
			print "Distinct ratio values: "
			print ratio_values
			if len(ratio_values) < 4:
				print "Annotating with "+str(ratio_values)
				axRatio.text(0, 3, "Distinct Values:\n"+ str(ratio_values))


	if show_timeslider:
		axcolor = 'lightgoldenrodyellow'
		ax1 = plt.axes([0.25, 0.90, 0.65, 0.03], axisbg=axcolor)

		timeslider = Slider(ax1, "Time", mintime, maxtime)
		timeslider.on_changed(draw)

		# hack: timeslider must be kept in scope,
		# cf issue https://github.com/matplotlib/matplotlib/issues/3105/
		globals().update({"slider": timeslider })

	draw()

	#dxy(data['THC'], 0)


	return data

#data = compare("press.x.asc", THCMHD)

rho = compare("rho.x.asc", THCMHD)
press = compare("press.x.asc", THCMHD)

for key in ["1.MHD","2.THC"]:
	x, p = dxy(press[key])
	y, rh = dxy(rho[key])
	assert(all(x == y))
	gamma = 2
	# p == kappa * rh^gamma
	kappa = p / rh**gamma
	print "Kappa for %s: %s" % (key, str(kappa))	


#eps_ratio=dxy(eps["2.THC"])[1]/dxy(eps["1.MHD"])[1]
#press_ratio=dxy(press["2.THC"])[1]/dxy(press["1.MHD"])[1]

#eps_ratio
#press_ratio

