#!/usr/bin/python
# python2 plotting of dumped scalars

from pylab import *
from glob import glob
from os import path
import warnings

# quantities: ener, ent, gam_euler, nbar, press, vel[0], vel[1], vel[2]

class LoreneNS:
	# column names
	col_domain, col_phi, col_theta, col_r, col_value = "l", "k", "j", "i", "scalar_value"


	def __init__(self, star_dir):
		self.star_dir = star_dir
		#self.filename = "%s/%s.txt" % (outdir,quantity)

		if not path.exists(star_dir) or not path.isdir(star_dir):
			raise ValueError("LoreneNS: '%s' is not a valid directory" % star_dir)

		if not self.quantities():
			warnings.warn("LoreneNS: '%s' does not contain any suitable ASCII data" % star_dir);

	def __repr__(self):
		return "LoreneNS(star_dir='%s')\nwith quantities %s" % (self.star_dir, ", ".join(self.quantities()))

	def quantities(self):
		return [path.splitext(path.basename(x))[0] for x in glob("%s/*.txt" % self.star_dir)];

	def quantity_filename(self, quantity):
		return "%s/%s.txt" % (self.star_dir,quantity)

	def data(self, quantity):
		filename = self.quantity_filename(quantity)
		return genfromtxt(filename, names=True, skip_header=2)

	def plane(self, quantity, phi=0.):
		# cut a plane as mask
		data = self.data(quantity)
		plane = data[data[self.col_phi]==phi]
		return plane

	def axis(self, quantity, phi=0., theta=0.):
		plane = self.plane(quantity, phi)
		axis = plane[plane[self.col_theta]==theta]
		r = unique(axis[self.col_r])
		values = axis[self.col_value]
		return (r,values)
	
	# Independent Plotting capabilities of the LORENE data

	def plot_xyplane(self, quantity, phi=0.):
		data = self.data(quantity)
		plane = self.plane(data, phi)
		theta = unique(plane[self.col_theta]) * 2*pi / max(plane[self.col_theta])
		r = unique(plane[self.col_r])
		max_r, max_theta = max(plane[self.col_theta]), max(plane[self.col_r])

		# assumes column order!
		plane_data = plane[self.col_value].reshape((max_r+1, max_theta+1)).transpose()

		fig, ax = plt.subplots(subplot_kw=dict(projection='polar'))
		pc = ax.pcolor(theta, r, plane_data)
		#contourf

		colorbar(pc)
		title("%s at phi=%f" (quantity, phi))
		#savefig("%s/xy-2d-%s.png" % (self.star_dir, quantity))

	def plot_radial(self, quantity, phi=0., theta=0.):
		data = self.data(quantity)
		plane = self.plane(data, phi)
		axis = self.axis(plane, theta)
		figure()
		r = unique(axis[self.col_r])
		axis_data = axis[self.col_value]
	
		plot(r, axis_data, "o-")
		ylabel(quantity)
		xlabel("Radius [Lorene units] (chi)")
		title(quantity)
		#savefig("%s/xy-1d-%s.png" % (self.star_dir, quantity))

