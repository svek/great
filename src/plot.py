# functions to really draw instances of LoreneNS and CactusNS together.

# just do stuff
from lorenens import LoreneNS
from cactusns import CactusNS
from pylab import *
from scipy.interpolate import interp1d

# surely NOT the same stars!
#cs = CactusNS("../loewe-thc/loewe-lorenethc16-1-9-data_asc_1D/")
#ls = LoreneNS("../lorene/star-enth0.713793-freq100.000000.ascii/")

cs = CactusNS("../loewe-compare-star-16feb/cactus-export/data/")
ls = LoreneNS("../loewe-compare-star-16feb/lorene-export/")

# axis scaling
ca_in_lo = 1.47664

def load(nicename, cacname=None, lorenename=None):
	if not cacname: cacname = nicename
	if not lorenename: lorenename = cacname
	quantity_name = nicename
	cx, cval = cs.axis(cacname, "x")
	lx, lval = ls.axis(lorenename)
	return (quantity_name, cx, cval, lx, lval, cacname, lorenename)

u = load("Pressure", "press")
#u = load("Velocity", "vel[0]")
# so wirklich liegen die Groessen nicht aufeinander:
#u = load("Particle number density", "dens", "nbar")
#u = load("Energy density", "dens", "ent")
#u = load("Epsilon", "eps", "ener") # passt nicht zusammen
#u = load("Epsilon", "dens", "ener")

quantity_name, cx, cval, lx, lval, cacname, lorenename = u

# scale lorene to cactus axis
#lx = lx * ca_in_lo

# strip the neg values
pos_indices = cx>=0
cx, cval = cx[pos_indices], cval[pos_indices]

c = interp1d(cx, cval, fill_value=0, bounds_error=False)
l = interp1d(lx, lval, fill_value=0, bounds_error=False)

x = linspace(0,90,num=120)

# now compare these data
ion(); clf()

title(quantity_name)
#plot(x, c(x), "-", label="Cactus")
#plot(cx, cval, "o", label="Cactus")
#plot(x, l(x), "-", label="Lorene")
plot(lx, lval, "-", label="Lorene (%d points)" % len(lval))

# interessante Sachen: Fitting x und y-Achse
def fit_cactus(x, xscale, yscale):
	return yscale * c(xscale * x)

from scipy.optimize import curve_fit

lorene_ydata = l(x)

# initial guesses
xscale, yscale = 1./10, 3720
popt, pcov = curve_fit(fit_cactus, x, lorene_ydata, [xscale, yscale])#, sigma=lorene_ydata/50.)

xscale, yscale = popt
xerr, yerr = pcov[0,0]**0.5, pcov[1,1]**0.5
cacfit = fit_cactus(x, xscale, yscale)

msg= "Fitted %s as %s of Cactus to %s of Lorene with\n" % (quantity_name, cacname, lorenename)
msg+= "  xscale=%f +- %f (%f %%)\n" % (xscale, xerr, xerr/xscale)
msg+= "  yscale=%f +- %f (%f %%)\n" % (yscale, yerr, yerr/yscale)

plot(x, cacfit, "o", label="Cactus fit to Lorene (%d points)" % len(cval))


print msg
legend()
figtext(0.5, 0.7, msg, bbox={'pad':10, 'facecolor':'grey'})

def compare_id(stars):
	clf()
	
	

	# get common times
	mintime = max([min(d["it"]) for d in data.values()])
	maxtime = min([max(d["it"]) for d in data.values()])
	times = unique(flatten([d["it"] for d in data.values()]))
	times = times[logical_and(times >= mintime, times <= maxtime)]

	print "For %s in %s-axis from times %f to %f:" % (fname, xaxis, mintime, maxtime)
	print times

	def find_nearest(a, a0):
		return a.flat[abs(a-a0).argmin()]

	# plot the first time and store line references
	gs = GridSpec(2,1, height_ratios=[3,1])
	axMain = subplot(gs[0])
	axRatio = subplot(gs[1])
	if show_timeslider:
		subplots_adjust(left=0.25, top=0.85)
	else:
		subplots_adjust(left=0.05, top=0.95, right=0.95, bottom=0.05)
	title(fname)
	lines = {}
	initial_it=0
	for star, d in data.iteritems():
		x,y = dxy(d, xaxis, initial_it)
		l, = axMain.plot(x,y, "o-", label=star)
		lines[star] = l


	axMain.legend()
	axRatio.set_xlabel(xaxis)


	def draw(it_ideal=0):
		it = find_nearest(times, it_ideal)
		#print "Drawing for time it=%f => it=%f" % (it_ideal, it)
		for star, d in data.iteritems():
			x,y = dxy(d, xaxis, it)
			if(len(y)):
				lines[star].set_ydata(y) #plot(x,y, "o-", label=star)
		axMain.set_title("%s at t=%f" % (fname, it))

		# draw the ratio between both
		first = data.keys()[0]
		second = data.keys()[1]
		#print "Ratio: Showing %s/%s" % (first,second)

		x,y1 = dxy(data[first], xaxis, it)
		x,y2 = dxy(data[second], xaxis, it)
		if len(y1) and len(y2):
			axRatio.clear()
			eps = 1e-10
			ratio = y1/y2
			#print "Ratio at it=%f: %s" % (it, str(ratio))
			#x, ratio = x[y1>eps], ratio[y1>eps]
			axRatio.plot(x,ratio, "o-", label="%s/%s"%(first,second))

			# senseful or not: upper limit
			ylim = max(ratio)
			ylim = 5

			axRatio.set_ylim([0,ylim])
			axRatio.legend()

			# print some values
			ratio_values = unique(ratio)
			print "Distinct ratio values: "
			print ratio_values
			if len(ratio_values) < 4:
				print "Annotating with "+str(ratio_values)
				axRatio.text(0, 3, "Distinct Values:\n"+ str(ratio_values))


	if show_timeslider:
		axcolor = 'lightgoldenrodyellow'
		ax1 = plt.axes([0.25, 0.90, 0.65, 0.03], axisbg=axcolor)

		timeslider = Slider(ax1, "Time", mintime, maxtime)
		timeslider.on_changed(draw)

		# hack: timeslider must be kept in scope,
		# cf issue https://github.com/matplotlib/matplotlib/issues/3105/
		globals().update({"slider": timeslider })

	draw()

	#dxy(data['THC'], 0)


	return data
