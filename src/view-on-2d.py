# just do stuff
from lorenens import LoreneNS
from cactusns import CactusNS
from pylab import *
from scipy.interpolate import interp1d
ion()

# surely NOT the same stars!
#cs = CactusNS("../loewe-thc/loewe-lorenethc16-1-9-data_asc_1D/")
#ls = LoreneNS("../lorene/star-enth0.713793-freq100.000000.ascii/")

cs = CactusNS("../loewe-compare-star-16feb/cactus-export/data/")
ls = LoreneNS("../loewe-compare-star-16feb/lorene-export/")

quantity = "vel[0]"

for plane in []:#["xy", "xz", "yz"]:
	d = cs.data("vel[0]", plane)
	coords, d2d = cs.dplane(d, plane[0], plane[1])

	figure()
	clf()
	title("ASC %s in %s plane" % (quantity, plane))
	imshow(d2d, interpolation="none", origin="lower")
	colorbar()


for plane in ["xy", "xz", "yz"]:
	d = cs.data_hdf5("vel[0]", plane)

	figure()
	clf()
	title("HDF5 %s in %s plane" % (quantity, plane))
	imshow(d, interpolation="none", origin="lower")
	colorbar()


for plane in ["xy", "xz", "yz"]:
	di = array([np.copy(cs.data_hdf5("vel[%i]" % i, plane)) for i in [0,1,2]])
	diS = swapaxes(di, 0, 2)
	vNorm = norm(diS, axis=2)

	# create stuff in increasing ax1 direction.
	for i1, v1 in enumerate(vNorm):
		vNorm[i1, 0] = i1 * np.max(vNorm)/len(vNorm)

	figure()
	clf()
	title("HDF5 NORM(vel[x,y,z]) in %s plane" % plane)
	imshow(vNorm, interpolation="none", origin="lower")
	colorbar()


