ion();

loewe="/run/user/1000/gvfs/sftp:host=loewe"

stars = {
	"THC": loewe+"/home/astro/koeppel/simulations/ng-codecomp/lorene-thc/data_asc_1D/",
	"MHD": loewe+"/home/astro/koeppel/simulations/ng-codecomp/lorene-mhd-newer/data_asc_1D/"
}

# downloaded files
stars = {
#	"THC": "./loewe-lorenethc-data_asc_1D/",
	"TCH": "./lthc15-01-11-3-data_asc_1D/",
	"MHD": "./loewe-mhd-newer-data_asc_1D/",
}

# another comparison input
THCMHD = {
#	"THC1": "./loewe-lorenethc-data_asc_1D/",
#	"THC1": "./loewe-lorenethc16-1-9-data_asc_1D/",
	"2.THC": "./lthc15-01-11-3-data_asc_1D/",
#	"1.MHD": "./loewe-mhd-newer-data_asc_1D/",
	"1.MHD": "./loewe-mhd16-1-11-data_sc_1D/",
}

THCrotating = {
# actually I do not even want to compare something. Just
# want to display the profiles of a rotating initial data read in.
#	"RTHC":  "./loewe-ngr-16-1-15-data/",
	"RTHC": "./loewe-16-01-20-fgt0-data/",
	"NRTHC": "./lthc15-01-11-3-data_asc_1D/",
}