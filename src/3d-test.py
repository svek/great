from pylab import *
from mpl_toolkits.mplot3d import Axes3D
ion()

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.set_zlim(0, 1)
ax.set_xlabel("x")
ax.set_ylabel("y")
ax.set_zlabel("z")


def plot_plane(point, normale):
	point, normale = np.array(point), np.array(normale)
	# a plane is a*x+b*y+c*z+d=0
	# [a,b,c] is the normal. Thus, we have to calculate
	# d and we're set
	d = -point.dot(normal)

	# create x,y
	xx, yy = np.meshgrid(range(10), range(10))

	# calculate corresponding z
	z = (-normal[0] * xx - normal[1] * yy - d) * 1. /normal[2]

	# the data to show
	data = z**2 + yy**2 + xx**2.
	N = data / data.max()

	# plot the surface
	theplot = ax.plot_surface(xx, yy, z, rstride=1, cstride=1, facecolors=cm.jet(N), antialiased=False, shade=False)
	cmap = theplot.get_cmap()
	plt.show()

	m = cm.ScalarMappable(cmap=cm.jet)
	m.set_array(data)
	plt.colorbar(m)

	# add a colorbar
	#ax2 = fig.add_axes([0.95, 0.1, 0.03, 0.8])
	#cb = mpl.colorbar.ColorbarBase(ax2, cmap=cmap, spacing='proportional')



point  = np.array([0, 0, 0])
normal = np.array([0, 0, 1])


zp,zn = [0,0,0], [0,0,1]
plot_plane(zp,zn)

xp, xn = [1,1,1], [1,0,5]
plot_plane(xp, xn)


