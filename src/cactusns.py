# Class representing cactus data

from re import sub, search
from os import path
from glob import glob
from pylab import *
from matplotlib.gridspec import GridSpec

# rho press dens eps alp vel[0] tau

class CactusNS:
	xaxis="x" # the default one

	# 1D ASCII Cactus output format:
	# time leve, refinement leve, multigrid level
	names="1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data"
	names = sub(r"[^a-z\s]", "", names).split()

	def dxy(self, d, xaxis="x", it=0):
		"""
		Extract 1D data
		"""
		mask = logical_and(d["it"]==it, d["rl"]==0, d["ml"]==0)
		x,y = d[mask][xaxis], d[mask]["data"]
		return (x,y)

	def __init__(self, star_dir):
		"""
		fname: Full name to ASCII data.
		xaxis: string value "x", "y" or "z", which data to display
		"""
		self.star_dir = star_dir

		if not path.exists(star_dir) or not path.isdir(star_dir):
			raise ValueError("CactusNS: '%s' is not a valid directory" % star_dir)

		if not self.quantities():
			warnings.warn("CactusNS: '%s' does not contain any suitable ASCII data" % star_dir);

	def __repr__(self):
		return "CactusNS(star_dir='%s')\nwith quantities %s" % (self.star_dir, ", ".join(self.quantities()))

	def quantities(self):
		return [path.basename(x) for x in glob("%s/*.asc" % self.star_dir)];

	def quantity_filename(self, quantity, axis):
		"""
		Axis like "x" or "y" or "z".
		Quantity like "alp".
		gives "alp.x.asc".
		Another example: "alp", "xy" for 2D data
		"""
		return "%s/%s.%s.asc" % (self.star_dir,quantity,axis)
	

	def data(self, quantity, xaxis):
		#extractAxis =  search(r"(x|y|z).asc", quantity)
		#if not xaxis and extractAxis:
		#	xaxis = extractAxis.group(1)
		#else:
		#	raise ValueError("Failed to extract axis from %s, please provide as arg." % fname)
		#
		fname = self.quantity_filename(quantity, xaxis)	
		return genfromtxt(fname, names=self.names)

	def data_hdf5(self, quantity, axis):
		"""
		hdf5 data has no weird spaces inbetween and is already 2D
		"""
		fname = "%s/%s.%s.h5" % (self.star_dir, quantity, axis)
		import h5py
		f = h5py.File(fname, 'r')
		keyname=u"HYDROBASE::%s it=0 tl=0 rl=0" % unicode(quantity)
		if not keyname in f:
			raise KeyError("Cannot find %s in %s" % (keyname, str(f)))
		return f[keyname]
	
	# just for compatibility
	def axis(self, quantity, axis=None):
		data = self.data(quantity, axis)
		return self.dxy(data, axis)

	def dplane(self, d, ax1="x", ax2="y", it=0):
		"""
		Extract 2D data (ie. a plane) from ASC data.
		"""
		mask = logical_and(d["it"]==it, d["rl"]==0, d["ml"]==0)
		dm = d[mask]
		indices = dm[[ax1, ax2]]
		# gather data as 2d
		#maxidx = {k: max(dm[k]) for k in ['ix','iy','iz']}
		#minidx = {k: min(dm[k]) for k in ['ix','iy','iz']}
		i1,i2 = 'i'+ax1, 'i'+ax2
		shape = [max(dm[k])+1 for k in [i1,i2]]
		data = zeros(shape)
		for line in dm:
			data[line[i1], line[i2]] = line['data']
		return (indices,  data)
	
		
	def times():
		# still written for data[star] = d
		# somewhat useless for single data

		# get common times
		mintime = max([min(d["it"]) for d in data.values()])
		maxtime = min([max(d["it"]) for d in data.values()])
		times = unique(flatten([d["it"] for d in data.values()]))
		times = times[logical_and(times >= mintime, times <= maxtime)]
